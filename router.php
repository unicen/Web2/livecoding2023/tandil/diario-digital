<?php
require_once 'noticias.php';
require_once 'about.php';

define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

// el router va a leer la action desde el paramtro "action"

$action = 'home'; // accion por defecto
if (!empty( $_GET['action'])) {
    $action = $_GET['action'];
}

// parsea la accion Ej: noticia/1 --> ['noticia', 1]
$params = explode('/', $action);

switch ($params[0]) { // en la primer posicion tengo la accion real
    case 'home':
        showNoticias(); // muestra todas las noticias
        break;
    case 'noticia':
        showNoticiaById($params[1]); // muestra una noticia
        break;
    case 'about':
        if (isset($params[1]))
            showAbout($params[1]);
        else 
            showAbout();
        break;
    default: 
        show404();
        break;
}