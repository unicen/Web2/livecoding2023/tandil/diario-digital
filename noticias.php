<?php
require_once 'db_fake.php';

function showNoticias() {
    include_once 'templates/header.php'; ?>

    <!-- main section -->
    <main class="container mt-5">
    <?php
        // obtengo el arreglo de noticias
        $noticias = getNoticias();
    ?>

    <section class="noticias">
        <?php foreach($noticias as $key => $noticia) { ?>
        <div class="card">
            <img src="<?php echo $noticia->imagen ?>" alt="...">
            <div class="card-body">
                <h5 class="card-title"><?php echo $noticia->titulo ?></h5>
                <p class="card-text"><?php  echo $noticia->contenido ?></p>
                <a href="noticia/<?php echo $key ?>" class="btn btn-outline-primary">Leer más</a>
            </div>
        </div>
        <?php } ?>
    </section>
    </main>

    <?php include_once 'templates/footer.php';
}

function showNoticiaById($id) {
    include_once 'templates/header.php';
    $noticia = getNoticiaById($id);
  ?>

    <main class="container mt-5">
    <section class="noticia">
        <h1 class="mb-5"><?php echo $noticia->titulo ?></h1>
        <img class="noticia-image" src="<?php echo $noticia->imagen ?>" alt="...">
        <p class="lead mt-3"><?php echo $noticia->contenido ?></p>
    </section>
    </main>

    <?php include_once 'templates/footer.php';
}

function show404() {
    include_once 'templates/header.php';
    echo "<h1 class='text-center mt-5 mb-5'>ERROR 404</h1>";
    include_once 'templates/footer.php';
}